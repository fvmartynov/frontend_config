var gulp = require('gulp');

var nib = require('nib');
var browserSync = require('browser-sync');
var less = require('gulp-less');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var htmlmin = require('gulp-htmlmin');
var dirSync = require('gulp-directory-sync');
var htmlLint = require('gulp-html-lint');
var imagemin = require('gulp-imagemin');
var csso = require('gulp-csso');
var sourcemaps = require('gulp-sourcemaps');


// Concat & Minify JS-files
gulp.task('buildJS', function() {
  return gulp.src('src/js/*.js')
    .pipe(concat('script.js'))
    .pipe(uglify())
    .pipe(gulp.dest('prod'));
});

// Compile LESS files
gulp.task('buildLESS', function() {
  return gulp.src('src/less/main.less')
    .pipe(less())
    .pipe(csso())
    .pipe(rename('style.css'))
    .pipe(gulp.dest('prod'));
});

// Build HTML file
gulp.task('buildHTML', function() {
  return gulp.src('src/index.html')
    .pipe(htmlmin({
      caseSensitive: true,
      collapseWhitespace: true
    }))
    .pipe(gulp.dest('prod'));
});

// Sync dir src/assets with prod/assets
gulp.task('syncAssets', function() {
  return gulp.src('')
    .pipe(dirSync('src/assets', 'prod/assets'));
});

// browser-sync server setup
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: "./prod"
    },
    ui: {
      port: 11231
    },
    port: 11230,
    open: true,
    notify: false,
    ghostMode: false
  });
});
// browser-sync reload task
gulp.task('reload', function() {
  return gulp.src('prod/*')
    .pipe(browserSync.reload({stream:true}));
});

// Main watch task
gulp.task('watch', function() {
  gulp.watch('src/js/*.js', ['buildJS']);
  gulp.watch('src/less/*.less', ['buildLESS']);
  gulp.watch('src/index.html', ['buildHTML']);
  gulp.watch('src/assets/**', ['syncAssets']);
  gulp.watch('prod/**', ['reload']);
});

// Build tasks
gulp.task('lint', []);
gulp.task('build-dev', ['buildJS', 'buildLESS', 'buildHTML', 'syncAssets']);
gulp.task('build-prod', ['buildJS', 'buildLESS', 'buildHTML', 'syncAssets']);

// Gulp Default task
gulp.task('default', ['build-dev', 'browserSync', 'reload', 'watch']);
