# README #

Даный репозиторий содержит базовые конфигурации и примеры для фронтендеров (для вёрстки).

### Основные данные ###

* frontend configs
* 1.0.0

### Содержнание ###

* Директория setup1 содержит сборку gulp+less+browser-sync

### Структура конфигурации ###

* setup#/src - исходные файлы
* setup#/prod - результирующие файлы
* setup#/gulpfile.js - конфигурация gulp
* setup#/package.json - конфигурация npm
* assets - дополнительные зависимости
* assets/css - дополнительные css (пример: bootstrap.min.css)
* assets/js - дополнительные js (пример: jquery.js)
* assets/img - изображения
* js - исходные js-файлы
* less - исходные less-файлы
* index.html - основной html-файл
